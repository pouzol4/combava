# Combava (Avril 2020 - Mai 2020)

_Ceci est un projet scolaire pour le module M2105 : Interface Homme-Machine. Le but de l'application était de faire un site "vitrine" sur une association qui proposait dans divers lieux des ventes de citronnades. Le but de celui-ci était de découvrir les problèmes que peut voir un développeur IHM._

## Consigne

Voici où vous pouvez trouver les consignes : [https://www.cloudschool.org/iut-info/travaux-pratiques](https://www.cloudschool.org/iut-info/travaux-pratiques)

## Comment utiliser le projet ?

Pour ce projet, on peut juste aller sur le lien : [http://combava.hadrien-pouzol.fr/](http://combava.hadrien-pouzol.fr/)

## Etat du projet 

Fini

## Langages

**FrontEnd :** HTML / CSS

## Auteurs

Hadrien POUZOL / Maxime CONSTANS


